using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour {
    // Start is called before the first frame update


    private RawImage rawImage;
    public Color crosshairColor = new Color(0, 255, 0, 255);
    public Color nonCrosshairColor = new Color(255, 255, 255, 255);
    public int crosshairThickness = 3;

    public void Start() {
        this.rawImage = GetComponent<RawImage>();
        this.DrawCrosshair();
    }

    public void DrawCrosshair() {
        var texture = new Texture2D(128, 128);
        for (int y = 0; y < texture.height; y++) {
            for (int x = 0; x < texture.width; x++) {
                if (Mathf.Abs(x - texture.width / 2) < crosshairThickness ||
                    Mathf.Abs(y - texture.height / 2) < crosshairThickness) {
                    texture.SetPixel(x, y, this.crosshairColor);
                }
                else {
                    texture.SetPixel(x, y, this.nonCrosshairColor);
                }
            }
        }


        texture.Apply();
        this.rawImage.texture = texture;
    }
}