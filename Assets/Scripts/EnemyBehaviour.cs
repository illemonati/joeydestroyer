﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class EnemyBehaviour : MonoBehaviour {
    // Start is called before the first frame update

    public float minDist = 1f;
    public float moveSpeed = 10f;
    public Vector3 screenPos;
    public int healthBarYOffset = 50;
    public GUIStyle healthBarTextStyle;
    public PlayerBehaviour[] players;
    public event EventHandler deathHandler;
    public event EventHandler spawnHandler;

    private Camera m_MainCamera;

    public void Start() {
        m_MainCamera = Camera.main;
        this.healthBarTextStyle = new GUIStyle {fontSize = 50};
        this.healthBarTextStyle.normal.textColor = Color.magenta;
        this.players = (GameObject.FindObjectsOfType(typeof(PlayerBehaviour)) as PlayerBehaviour[]);
        foreach (var player in players) {
            // player.health = player.maxHealth;
            // player.antiJoeyPower = player.maxAntiJoeyPower;
            deathHandler += player.OnEnemyDeath;
            spawnHandler += player.OnEnemySpawn;
        }

        spawnHandler?.Invoke(this, EventArgs.Empty);
    }

    public void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Wall")) {
            Debug.Log("collided with wall");
            this.Die();
        }
    }

    public void Die() {
        Destroy(gameObject, 0f);

        deathHandler?.Invoke(this, EventArgs.Empty);
    }


    // Update is called once per frame
    public void Update() {
        if (!m_MainCamera) return;
        screenPos = m_MainCamera.WorldToScreenPoint(transform.position);

        transform.LookAt(m_MainCamera.transform);

        if (Vector3.Distance(transform.position, m_MainCamera.transform.position) >= minDist) {
            var transform1 = transform;
            transform1.position += transform1.forward * (this.moveSpeed * Time.deltaTime);
        }
    }


    public void OnGUI() {
        if (screenPos.x < 0 || screenPos.y < 0 || screenPos.x > Screen.width || screenPos.y > Screen.height ||
            screenPos.z < 0) {
            return;
        }

        var textSize = GUI.skin.label.CalcSize(new GUIContent("JOEY"));
        // GUI.Label(new Rect(screenPos.x,screenPos.y,300,150),"JOEY", this.healthBarTextStyle);
        GUI.Label(new Rect(screenPos.x, Screen.height - screenPos.y, textSize.x, textSize.y), "JOEY",
            this.healthBarTextStyle);
    }
}