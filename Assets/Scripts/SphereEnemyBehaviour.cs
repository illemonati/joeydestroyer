using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereEnemyBehaviour : EnemyBehaviour {
    // No move
    public float sphereMinDist = 100000f;
    public float timeDelay = 3f;
    public GameObject CylinderEnemy;
    public int maxCylindersAlive = 5;
    private int cylindersAlive = 0;
    //public event EventHandler enemySpawnHandler;
    public new void Start() {
        base.Start();
        base.minDist = sphereMinDist;
        InvokeRepeating(nameof(SpawnCylinderEnemy), timeDelay, 1f);
        this.cylindersAlive = 0;
        // foreach (var player in players) {
        //     enemySpawnHandler += player.OnEnemySpawn;
        // }
    }

    public void OnSpawnedCylinderDeath(object sender, EventArgs e) {
        this.cylindersAlive--;
    }

    public void SpawnCylinderEnemy() {
        if (this.cylindersAlive >= maxCylindersAlive) return;
        var direction = transform.forward;
        var directionInQuaternion = Quaternion.Euler(direction.x, direction.y, direction.z);
        var cylinder = Instantiate(CylinderEnemy, transform.position + directionInQuaternion * direction * 2f,
            directionInQuaternion) as GameObject;
        
        cylinder.GetComponent<Rigidbody>().AddForce(direction * 10);
        this.cylindersAlive++;
        // enemySpawnHandler?.Invoke(this, EventArgs.Empty);
    }
}
