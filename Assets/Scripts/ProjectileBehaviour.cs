using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour {
    public float secsBeforeRemoval = 20f;

    public void Start() {
        Destroy(gameObject, secsBeforeRemoval);
    }
}