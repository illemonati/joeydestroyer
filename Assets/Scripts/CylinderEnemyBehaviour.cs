using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class CylinderEnemyBehaviour : EnemyBehaviour {

    public float cylinderMinDist = 100f;
    public GameObject projectile;
    public AudioClip shootSound;
    private AudioSource m_AudioSource;
    
    private new void Start() {
        base.Start();
        base.minDist = cylinderMinDist;
        InvokeRepeating(nameof(Shoot), 3f, 1f);
        m_AudioSource = GetComponent<AudioSource>();
    }


    private void Shoot() {
        var direction = transform.forward;
        var directionInQuaternion = Quaternion.Euler(direction.x, direction.y, direction.z);
        var bullet = Instantiate(projectile, transform.position + directionInQuaternion * direction * 2f,
                directionInQuaternion) as GameObject;
        bullet.GetComponent<Rigidbody>().AddForce(direction * 5000);
        m_AudioSource.clip = shootSound;
        m_AudioSource.Play();
    }


    private new void Update() {
        base.Update();
    }
}