﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = System.Object;

[RequireComponent(typeof(Rigidbody))]
public class PlayerBehaviour : MonoBehaviour {
    // Start is called before the first frame update

    private Camera m_PlayerCamera;
    private Rigidbody m_PlayerBody;
    private AudioSource m_AudioSource;
    private Vector2 mouseLook;
    private Vector2 mouseSmoothV;
    public AudioClip hurtSound;
    public AudioClip shootSound;
    
    public bool isGrounded;

    public float mouseSensitivity = 3.0f; //Mouse sensitivity.
    public float mouseSmoothing = 2.0f;

    public string[] enemyTypes = {"CubeEnemy", "SphereEnemy", "CylinderEnemy"};
    public float viewRange = 60.0f;
    public Text healthAndAntiJoeyPower;
    public Text bannerMessage;
    public float timeForBannerMessage = 3;
    public int maxHealth = 100;
    public int health;
    public int maxAntiJoeyPower = 200;
    public int antiJoeyPower;
    public float speed = 50f;
    public int projectileCost = 10;
    public float respwanSeconds = 3;
    public GameObject projectile;
    public int amountEnemies = 0;
    private object amountEnemiesSemaphore = new object();
    private int currentStage = 0;

    private void Start() {

        if (PlayerPrefs.HasKey("CurrentStage")) {
            currentStage = PlayerPrefs.GetInt("CurrentStage");
            if (currentStage > SceneManager.sceneCountInBuildSettings - 1) {
                currentStage = SceneManager.sceneCountInBuildSettings - 1;
                PlayerPrefs.SetInt("CurrentStage", currentStage);
            } 
            if (SceneManager.GetActiveScene().buildIndex != currentStage) {
                SceneManager.LoadScene(currentStage);
            }
        } else {
            currentStage = SceneManager.GetActiveScene().buildIndex;
            PlayerPrefs.SetInt("CurrentStage", currentStage);
        }

        // lock (amountEnemiesSemaphore) {
        //     amountEnemies = 0;
        //     foreach (var enemyType in enemyTypes) {
        //         amountEnemies += GameObject.FindGameObjectsWithTag(enemyType).Length;
        //     }
        // }
        
        this.SetBannerMessage($"Stage {currentStage} - start");
        
        this.m_PlayerCamera = gameObject.GetComponent<Camera>();
        this.m_AudioSource = gameObject.GetComponent<AudioSource>();
        this.m_PlayerBody = gameObject.GetComponent<Rigidbody>();
        this.health = this.maxHealth;
        this.antiJoeyPower = this.maxAntiJoeyPower;
        this.SetHealthAndAntiJoeyPower();
        Cursor.visible = false;
        // Debug.Log(m_PlayerCamera);
        Cursor.lockState = CursorLockMode.Locked;
        this.isGrounded = false;
    }

    public IEnumerator LoadNextStage(int stageNumber) {
        yield return new WaitForSeconds(timeForBannerMessage);
        currentStage = stageNumber;
        PlayerPrefs.SetInt("CurrentStage", currentStage);
        SceneManager.LoadScene(currentStage);
    }

    public void OnEnemySpawn(object sender, EventArgs e) {
        Debug.Log("Enemy Spawned");
        lock (amountEnemiesSemaphore) {
            this.amountEnemies++;
        }
    }

    public void OnEnemyDeath(object sender, EventArgs e) {
        Debug.Log("Enemy Died");
        this.health = this.maxHealth;
        this.antiJoeyPower = this.maxAntiJoeyPower;

        lock (amountEnemiesSemaphore) {
            this.amountEnemies--;
        }
        Debug.Log($"{amountEnemies} left");
        SetBannerMessage($"Enemy Killed - {amountEnemies} left");
        Debug.Log($"{SceneManager.sceneCount}");
        if (this.amountEnemies < 1) {
            Debug.Log($"{currentStage} win");
            Debug.Log($"{currentStage}");
            SetBannerMessage($"Stage {currentStage} win");
            if (SceneManager.sceneCountInBuildSettings > currentStage) {
                StartCoroutine(this.LoadNextStage(this.currentStage+1));
            } else {
                SetBannerMessage($"Game Completed\nPress Z to restart", noClear: true);
            }
        }
    }

    public IEnumerator ClearBannerMessage(string message) {
        yield return new WaitForSeconds(timeForBannerMessage);
        if (bannerMessage.text == message) {
            bannerMessage.text = "";
        }
    }

    public void SetBannerMessage(string message, bool noClear = false) {
        if (!bannerMessage) return;
        bannerMessage.text = message;
        if (!noClear) StartCoroutine(this.ClearBannerMessage(message));
    }

    private bool GameCompleted() {
        return amountEnemies < 1 && this.currentStage >= SceneManager.sceneCountInBuildSettings - 1;
    }


    private void LookAround() {
        if (this.health < 1) return;
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        md = Vector2.Scale(md, new Vector2(this.mouseSensitivity * this.mouseSmoothing, this.mouseSensitivity * this.mouseSmoothing));
        mouseSmoothV.x = Mathf.Lerp(mouseSmoothV.x, md.x, 1f / this.mouseSmoothing);
        mouseSmoothV.y = Mathf.Lerp(mouseSmoothV.y, md.y, 1f / this.mouseSmoothing);
        mouseLook += mouseSmoothV;
        var playerCameraTransform = this.m_PlayerCamera.transform;
        playerCameraTransform.localEulerAngles = new Vector3(-mouseLook.y, playerCameraTransform.localEulerAngles.y, 0);
        var playerTransform = transform;
        playerTransform.localEulerAngles = new Vector3(playerTransform.localEulerAngles.x, mouseLook.x, 0);
    }


    private IEnumerator WaitForRespawn() {
        yield return new WaitForSeconds(this.respwanSeconds);
        this.health = this.maxHealth;
        SceneManager.LoadScene(0);
    }


    private void OnCollisionStay() {
        this.isGrounded = true;
    }


    public void GetHurt(int damage) {
        health -= damage;
        m_AudioSource.clip = hurtSound;
        m_AudioSource.Play();
        // Debug.Log(m_AudioSource.volume);
        // Debug.Log(m_AudioSource.clip);
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("CubeEnemy")) {
            Debug.Log("collided with enemy");
            GetHurt(10);
        } else if (collision.gameObject.CompareTag("Wall")) {
            Debug.Log("collided with wall");
            this.Die();
        } else if (collision.gameObject.CompareTag("CylinderProjectile")) {
            Debug.Log("collided with cylinder");
            GetHurt(10);
        }
    }


    private void SetHealthAndAntiJoeyPower() {
        if (!healthAndAntiJoeyPower) return;
        this.healthAndAntiJoeyPower.text =
            $"{"Health:".PadRight(20, ' ')}{health.ToString().PadLeft(5, ' ')} / {maxHealth.ToString().PadLeft(5, ' ')}\n{"AntiJoeyPower:".PadRight(20, ' ')}{antiJoeyPower.ToString().PadLeft(5, ' ')} / {maxAntiJoeyPower.ToString().PadLeft(5, ' ')}\nStage: {currentStage}\nenemies: {amountEnemies}";
        // Debug.Log(this.healthAndAntiJoeyPower.text);
    }


    private void ShootProjectile() {
        if (this.health < 1) return;

        var direction = this.m_PlayerCamera.transform.forward;
        var directionInQuaternion = Quaternion.Euler(direction.x, direction.y, direction.z);

        if (Input.GetMouseButtonDown(0) && this.antiJoeyPower >= projectileCost) {
            var bullet = Instantiate(projectile, transform.position + directionInQuaternion * direction * 2f,
                directionInQuaternion) as GameObject;
            bullet.GetComponent<Rigidbody>().AddForce(direction * 5000);
            this.antiJoeyPower -= projectileCost;
            m_AudioSource.clip = shootSound;
            m_AudioSource.Play();
        }
    }


    private void MoveAround() {
        var playerDirection = transform.forward;

        playerDirection.y = 0;

        if (this.health < 1) return;

        if (Input.GetKeyDown(KeyCode.R)) {
            this.Die();
        }

        if (Input.GetKey(KeyCode.W)) {
            transform.position += Time.deltaTime * speed * playerDirection;
        }

        if (Input.GetKey(KeyCode.A)) {
            transform.position += Quaternion.Euler(0, -90, 0) * playerDirection * (Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.D)) {
            transform.position += Quaternion.Euler(0, 90, 0) * playerDirection * (Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.S)) {
            transform.position += Quaternion.Euler(0, 180, 0) * playerDirection * (Time.deltaTime * speed);
        }

        if (Input.GetKey(KeyCode.Z) && GameCompleted()) {
            currentStage = 0;
            PlayerPrefs.SetInt("CurrentStage", currentStage);
            SceneManager.LoadScene(currentStage);
        }

        if (Input.GetKey(KeyCode.Space)) {
            if (!this.isGrounded) return;
            var jump = new Vector3(0.0f, 10.0f, 0.0f);
            m_PlayerBody.AddForce(jump, ForceMode.Impulse);
            isGrounded = false;
        }
    }

    private void Die() {
        this.health = 0;
    }

    // Update is called once per frame
    private void Update() {
        this.MoveAround();
        this.ShootProjectile();
    }

    private void LateUpdate() {
        this.LookAround();
        if (transform.position.y < -100) {
            this.Die();
        }

        this.SetHealthAndAntiJoeyPower();
        if (this.health < 1) {
            StartCoroutine(this.WaitForRespawn());
        }
    }
}